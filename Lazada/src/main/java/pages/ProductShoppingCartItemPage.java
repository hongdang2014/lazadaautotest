package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import utils.Helper;

public class ProductShoppingCartItemPage extends CommonPage {
	private WebElement model;
	private WebElement title;
	private WebElement price;
	private Select count;
	private WebElement subtotal;
	
	
	public ProductShoppingCartItemPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement getModel() {
		return model;
	}
	public void setModel(WebElement model) {
		this.model = model;
	}
	public WebElement getTitle() {
		return title;
	}
	public void setTitle(WebElement title) {
		this.title = title;
	}
	public WebElement getPrice() {
		return price;
	}
	public void setPrice(WebElement price) {
		this.price = price;
	}
	public Select getCount() {
		return count;
	}
	public void setCount(Select count) {
		this.count = count;
	}
	public WebElement getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(WebElement subtotal) {
		this.subtotal = subtotal;
	}
	
	/**
	 * 
	 * @return
	 * Get url of product page of a product.
	 */
	public String getSrcUrlOfProduct(){
		return model.getAttribute("href");
	}
	
	/**
	 * 
	 * @return
	 * Get product description display on shopping cart page.
	 */
	public String getProductDescription(){
		return title.getText();
	}
	
	
	/**
	 * 
	 * @return
	 * Get product price display on shopping cart page.
	 */
	public String getProductPrice(){
		return price.getText();
	}
	
	
	/**
	 * 
	 * @return
	 * Get amount of product user chose to add into shopping cart
	 */
	public String getNumberOfProduct(){
		return this.count.getFirstSelectedOption().getText();
	}
	
	
	/**
	 * 
	 * @param value
	 * Change amount of product user chose.
	 */
	public void setNumberOfProduct(String value){
		this.count.selectByVisibleText(value);
	}

	
	/**
	 * 
	 * @return
	 * Return total price of amount product (price*price per item).
	 */
	public long getSubtotalProductPrice(){
		return Helper.splitPriceFromText(this.subtotal.getText());
	}
	
	
	/**
	 * 
	 * @return
	 * Verify whether total price is counted correctly.
	 */
	public boolean validateTotalOfProductPrice(){
		long pricePerItem = Helper.splitPriceFromText(getProductPrice());
		int numberOfItem = Integer.parseInt(getNumberOfProduct());
		
		if((pricePerItem * numberOfItem) == getSubtotalProductPrice()){
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public boolean isOpening() {
		// Do nothing
		return false;
	}

	@Override
	public void waitForPageLoad() {
		// Do nothing
		
	}
	
	
}
