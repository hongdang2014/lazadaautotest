package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import testcases.common.InitTest;
import utils.Constants;
import utils.Helper;

public class ProductPage extends CommonPage{
	//Product title
	private By titleSelector;
	//Product price
	private By priceSelector;
	private By buyBtn;


	
	public ProductPage(WebDriver driver) {
		super(driver);
		titleSelector = Helper.getBySelector(Constants.PRODUCT_TITLE);
		priceSelector = Helper.getBySelector(Constants.PRODUCT_PRICE);
		buyBtn =  Helper.getBySelector(Constants.PRODUCT_BUY_BTN);
	}
	
	/**
	 * 
	 * @return product title
	 */
	public String getProductTitle(){
		return getElement(titleSelector).getText();
	}
	
	/**
	 * 
	 * @return product price
	 */
	public String getProductPrice(){
		return getElement(priceSelector).getText();
	}
	
	
	public void clickBuyBtn(){
		clickBtn(buyBtn);
	}
	
	
	/**
	 * Click 'Buy' button to purchase a product.
	 */
	public ShoppingCartPage buyProduct(){
		ShoppingCartPage cartPage = new ShoppingCartPage(InitTest.driver);
		
		//Turn off the advertiser
		try {
			driver.findElement(Helper.getBySelector(Constants.PRODUCT_AD)).click();
		}catch(org.openqa.selenium.NoSuchElementException ex){
			//There is no ad.
		}
		
		//Click 'Buy' button.
		clickBuyBtn();
		//Wait for page finish adding product to shopping cart.
		waitForPageReload();
		
		if(cartPage.isOpening()){
			return cartPage;
		}
		else{
			return null;
		}
		
	}
	
	
	
	
	
	/**
	 * 
	 * @return
	 * Check whether product page is opening or not.
	 */
	@Override
	public boolean isOpening(){
		try{
			driver.findElement(Helper.getBySelector(Constants.PRODUCT_TITLE));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}
	
	
	/**
	 * Wait for page finish loading.
	 */
	@Override
	public void waitForPageLoad(){
		waiter.until(ExpectedConditions.visibilityOfElementLocated
				(Helper.getBySelector(Constants.PRODUCT_TITLE)));
	}

}
