package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import utils.Constants;
import utils.Helper;

public class SignUpPage extends CommonPage {
	private By nameSelector;
	private By emailSelector;
	private By passSelector;
	private By confirmPassSelector;
	private By sendBtn;
	private By errorMsgSelector;


	public SignUpPage(WebDriver driver) {
		super(driver);
		nameSelector = Helper.getBySelector(Constants.SIGN_UP_NAME);
		emailSelector = Helper.getBySelector(Constants.SIGN_UP_EMAIL);
		passSelector = Helper.getBySelector(Constants.SIGN_UP_PASSWORD);
		confirmPassSelector =  Helper.getBySelector(Constants.SIGN_UP_CONFIRMPASS);
		sendBtn =  Helper.getBySelector(Constants.SIGN_UP_SENDBTN);
		errorMsgSelector = Helper.getBySelector(Constants.SIGN_UP_ERR_MSGS);
	}
	
	/**
	 * 
	 * @param name
	 * Input test data into 'Name'
	 */
	public void sendDataIntoNameField (String name){
		getElement(nameSelector).sendKeys(name);
	}
	
	/**
	 * 
	 * @param email
	 * Input test data into 'Email'
	 */
	public void sendDataIntoEmailField (String email){
		getElement(emailSelector).sendKeys(email);
	}
	
	/**
	 * 
	 * @param pass
	 * Input test data into 'Password'
	 */
	public void sendDataIntoPassField (String pass){
		getElement(passSelector).sendKeys(pass);
	}
	
	/**
	 * 
	 * @param confirmPass
	 * Input test data into 'Confirm Password'
	 */
	public void sendDataIntoConfirmPassField (String confirmPass){
		getElement(confirmPassSelector).sendKeys(confirmPass);
	}
	
	/**
	 * Click 'Submit' button.
	 */
	public void clickSendBtn (){
		getElement(sendBtn).click();
	}
	
	
	public void clearDataInConfirmPassField(){
		getElement(confirmPassSelector).clear();
	}
	
	public void clearDataInNameField(){
		getElement(nameSelector).clear();
	}
	
	public void clearDataInEmailField(){
		getElement(emailSelector).clear();
	}
	
	public void clearDataInPasswordField(){
		getElement(passSelector).clear();
	}
	
	
	/**
	 * Clear inputed test data in fields.
	 */
	public void clearFormData(){
		clearDataInNameField();
		clearDataInEmailField();
		clearDataInPasswordField();
		clearDataInConfirmPassField();
	}
	
	
	/**
	 * 
	 * @return
	 * Check the present of require error message on the form.
	 */
	public boolean checkDisplayOfRequiredErrorMsgForConfirmPass(){
		try {
			String requiredErrMsg = getListElement(errorMsgSelector).get(3).getText();
			if(!Helper.getPropertyValue(Constants.SIGN_UP_REQ_MSG).equals(requiredErrMsg)){
				return false;
			}
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	
	/**
	 * 
	 * @return
	 * Check whether 'Sign Up' page is opening or not.
	 */
	@Override
	public boolean isOpening(){
		try{
			driver.findElement(Helper.getBySelector(Constants.WINDOW_POPUP));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}
	
	
	/**
	 * Reset test environment after running test cases.
	 */
	public void resetTestEnvironment(){
		//Close popup
		if(isOpening()){
			getElement(Helper.getBySelector(Constants.WINDOW_POPUP_CLOSE_BTN)).click();
		}
	}
	
	
	@Override
	public void waitForPageLoad() {
		waiter.until(ExpectedConditions.visibilityOfElementLocated
				(Helper.getBySelector(Constants.SIGN_UP_PAGE_LABEL)));
		
	}


}
