package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Helper;


/**
 * 
 * @author hongdang
 *
 */
public abstract class CommonPage {
	protected WebDriver driver;
	protected WebDriverWait waiter;
    
    
    
    
    /**
     * 
     * @param driver
     */
    public CommonPage(WebDriver driver){
    	this.driver = driver;
		this.waiter = new WebDriverWait(driver,15);
    }
    
	
	
    /**
     * @param data
     * Data fill into component
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * Purpose
     * Fill data into specific input component.
     */
    public void sendDataIntoField(String data, String selectorKey){
    	By bySelector = Helper.getBySelector(selectorKey);
    	if(bySelector != null){
    		waiter.until(ExpectedConditions.visibilityOfElementLocated(bySelector));
        	
        	WebElement e = driver.findElement(bySelector);
        	e.sendKeys(data);
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * Purpose
     * Click button captured by selectorKey.
     */
    public void clickBtn (By bySelector){
    	if(bySelector != null){
    		waiter.until(ExpectedConditions.elementToBeClickable(bySelector));
        	
        	WebElement e = driver.findElement(bySelector);
        	e.click();
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * Purpose
     * Clear data of specific input component.
     */
    public void clearData (String selectorKey){
    	By bySelector = Helper.getBySelector(selectorKey);
    	if(bySelector != null){
    		waiter.until(ExpectedConditions.visibilityOfElementLocated(bySelector));
        	
        	WebElement e = driver.findElement(bySelector);
        	e.clear();
    	}else{
    		throw new TimeoutException();
    	}
    }
    	
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * @return
     * WebElement captured by selectorKey
     */
    public WebElement getElement(By bySelector){
    	if(bySelector!= null){
    		waiter.until(ExpectedConditions.visibilityOfElementLocated(bySelector)); 
        	return driver.findElement(bySelector);
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * @return
     * WebElement captured by selectorKey
     */
    public WebElement getElement(WebElement parent,By bySelector){
    	if(bySelector!= null){
    		waiter.until(ExpectedConditions.visibilityOfElementLocated(bySelector)); 
        	return parent.findElement(bySelector);
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * @return
     * List of WebElements captured by selectorKey.
     */
    public List<WebElement> getListElement(By bySelector){
    	if(bySelector != null){
    		waiter.until(ExpectedConditions.presenceOfAllElementsLocatedBy(bySelector));
        	
        	return driver.findElements(bySelector);
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    /**
     * 
     * @param selectorKey
     * Key from Constant, load selector from properties file.
     * 
     * @return
     * List of WebElements captured by selectorKey.
     */
    public List<WebElement> getListElement(WebElement parent,By bySelector){
    	if(bySelector != null){
    		waiter.until(ExpectedConditions.presenceOfAllElementsLocatedBy(bySelector));
        	
        	return parent.findElements(bySelector);
    	}else{
    		throw new TimeoutException();
    	}
    }
    
    
    /**
	 * 
	 * @return
	 * Wait for Page reload after ajax call.
	 */
	public Boolean isRefeshPageStopped(){
		return (Boolean)((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
	}
	
	
	/**
	 * Check whether refresh page process stopped or not
	 */
	public void waitForPageReload(){
		while(true){
			if(isRefeshPageStopped()){
				break;
			}
		}
	}
	
	
	/**
	 * 
	 * @return
	 * Check whether a page is opening or not
	 * This method will be implemented at a specific page.
	 */
	public abstract boolean isOpening();
	
	
	
	/**
	 * Wait for page finish loading.
	 * This method will be implemented at a specific page.
	 */
	public abstract  void waitForPageLoad();
    
    
    
	
}
