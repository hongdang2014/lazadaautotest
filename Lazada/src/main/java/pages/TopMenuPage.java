package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;

import testcases.common.InitTest;
import utils.Constants;
import utils.Helper;

/**
 * 
 * @author hongdang
 * This page represent of top menu.
 *
 */
public class TopMenuPage extends CommonPage{
	private By registerFormSelector =  Helper.getBySelector(Constants.REGISTER_FORM_LINK);
	private By cartPageSelector = Helper.getBySelector(Constants.CART_PAGE_LINK);

	
	public TopMenuPage(WebDriver driver) {
		super(driver);
	}
	
	
	/**
	 * 
	 * @return
	 * Open 'Sign Up' form
	 */
	public SignUpPage openNewAccountForm(){
		SignUpPage signUpPage = new SignUpPage(InitTest.driver);
		
		if(!signUpPage.isOpening()){
			getElement(registerFormSelector).click();
			try {
				signUpPage.waitForPageLoad();
				return signUpPage;
			}catch(TimeoutException e){
				return null;
			}
		}else{
			return signUpPage;
		}
		
	}
	
	
	/**
	 * 
	 * @return
	 * Open 'Shopping Cart' page.
	 */
	public ShoppingCartPage openCartPage(){
		ShoppingCartPage cartPage = new ShoppingCartPage(InitTest.driver);
		
		if(!cartPage.isOpening()){
			getElement(cartPageSelector).click();
			try {
				cartPage.waitForPageLoad();
				return cartPage;
			}catch(TimeoutException e){
				return null;
			}
			
		} else {
			return cartPage;
		}
	}

	
	/**
	 * 
	 * @param url
	 * Navigate browser to a specific product page.
	 */
	public ProductPage openProductPage(String url){
		ProductPage productPage = new ProductPage(InitTest.driver);
		
		driver.navigate().to(url);
		productPage.waitForPageLoad();
		
		if(productPage.isOpening()){
			return productPage;
		}else{
			return null;
		}
	}


	@Override
	public boolean isOpening() {
		// Do nothing
		return false;
	}


	@Override
	public void waitForPageLoad() {
		// Do nothing
		
	}
}
