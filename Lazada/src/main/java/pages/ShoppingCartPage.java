package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import utils.Constants;
import utils.Helper;

public class ShoppingCartPage extends CommonPage{
	private List<ProductShoppingCartItemPage> products;
	private long sub;
	private long total;
	
	
	public ShoppingCartPage(WebDriver driver) {
		super(driver);
	}
	
	public List<ProductShoppingCartItemPage> getProducts() {
		return products;
	}


	public void setProducts(List<ProductShoppingCartItemPage> products) {
		this.products = products;
	}


	public long getSub() {
		return sub;
	}


	public void setSub(long sub) {
		this.sub = sub;
	}


	public long getTotal() {
		return total;
	}


	public void setTotal(long total) {
		this.total = total;
	}

	
	/**
	 * Fetch product list on shopping cart page to
	 * refresh web elements after changing html structure.
	 */
	public void loadProductsElement(){
		products = new ArrayList<ProductShoppingCartItemPage>();
		
		//Fetch web element of all products and initialize list of ProductShoppingCartItemPage object.
		List<WebElement> webElemntProducts = getListElement(Helper.getBySelector(Constants.SHOPPING_CART_PRODUCT_LIST));
		for(WebElement e : webElemntProducts){
			List<WebElement> cols = getListElement(e, By.tagName("td"));
			ProductShoppingCartItemPage p = new ProductShoppingCartItemPage(driver);
			//Set title of product
			p.setTitle(getElement(cols.get(1),Helper.getBySelector(Constants.SHOPPING_CART_ITEM_DES)));
			//Set price of product
			p.setPrice(getElement(cols.get(2),Helper.getBySelector(Constants.SHOPPING_CART_ITEM_PRICE)));
			//Set number of product
			p.setCount(new Select(getElement(cols.get(3),Helper.getBySelector(Constants.SHOPPING_CART_ITEM_COUNT))));
			//Set total of money of purchase products.
			p.setSubtotal(cols.get(4));
			
			products.add(p);
		}
		
		List<WebElement> subAndTotal = getListElement(Helper.getBySelector(Constants.SHOPPING_CART_SUB_TOTAL));
		//Set sub money on shopping cart page
		sub = Helper.splitPriceFromText(subAndTotal.get(0).getText());
		//set total money on shopping cart page
		total =  Helper.splitPriceFromText(subAndTotal.get(1).getText());
		
	}
	
	
	/**
	 * 
	 * @return boolean
	 * Verify whether sub and total money on shopping cart is counted correctly.
	 */
	public boolean validateShoppingCartTotalPrice(){
		long totalProductsPrice = 0;
		for (ProductShoppingCartItemPage product : products){
			totalProductsPrice += product.getSubtotalProductPrice();
		}
		
		if((totalProductsPrice == sub) && (totalProductsPrice == total)){
			return true;
		}else{
			return false;
		}
		
		
	}
	
	
	/**
	 * 
	 * @return
	 * Check whether shopping cart page is opening or not.
	 */
	@Override
	public boolean isOpening(){
		try{
			driver.findElement(Helper.getBySelector(Constants.WINDOW_POPUP));
			return true;
		}catch(NoSuchElementException e){
			return false;
		}
	}


	/**
	 * 
	 * @param topeMenuPage
	 * Reset environment after testcases.
	 */
	public void resetTestEnvironment(TopMenuPage topeMenuPage) {
		ShoppingCartPage cartPage = topeMenuPage.openCartPage();
		
		if(cartPage != null){
			//Delete chosen products in shopping cart.
			WebElement deleteLink = getElement(Helper.getBySelector(Constants.SHOPPING_CART_DELETE_LINKS));
			while(true){
				deleteLink.click();
				waitForPageReload();
				try {
					deleteLink = getElement(Helper.getBySelector(Constants.SHOPPING_CART_DELETE_LINKS));
				}catch(TimeoutException e){
					break;
				}
				catch(NoSuchElementException ex){
					break;
				}
			}
			
			//Close popup
			waitForPageReload();
			getElement(Helper.getBySelector(Constants.WINDOW_POPUP_CLOSE_BTN)).click();
		}
	}
	
	
	/**
	 * Wait until Shopping Cart Page finish loading.
	 */
	@Override
	public void waitForPageLoad(){
		waiter.until(ExpectedConditions.visibilityOfElementLocated
				(Helper.getBySelector(Constants.SHOPPING_CART_NOTIFY_MSG)));
	}
	
	
	
	
}
