package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.log4testng.Logger;


/**
 * 
 * @author hongdang
 *
 */
public class Helper {
	private static Logger logger = Logger.getLogger(Helper.class);
	private static final Properties PROPERTIES = new Properties();
	
	/**
	 * Purpose
	 * Load file config.properties
	 */
	private static void loadProperties() {
		String resourceName = "config.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		InputStream resourceStream = loader.getResourceAsStream(resourceName);
		
		try {
		    PROPERTIES.load(resourceStream);
		} catch (IOException e) {
			logger.error("Error while reading properties file");
		    throw new IllegalArgumentException("Error while reading properties file", e);
		}
	 }
	
	
	/**
     * 
     * @param key
     * @return
     * @throws IOException
     */
    public static String getPropertyValue(String key) {
	if(PROPERTIES.isEmpty()) {
	    loadProperties();
	}
	return PROPERTIES.getProperty(key);
    }
	
	
	
	/**
	 * Purpose
	 * Get reset environment configuration.
	 * Default value : false.
	 * @return
	 * true : if we need reset environment before running test case
	 * false : if we don't reset environment before running test case.
	 * @throws IOException
	 */
	public static boolean getResetTestEnviromentConfig(){
		return  Boolean.parseBoolean(getPropertyValue(Constants.RESET_TEST_ENV));
		
	}
	
	
	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String[] getStringArrayProperty(String key) {
		String s = getPropertyValue(key);
		if(s != null) {
		    return s.trim().split(",");
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param driver
	 * @param selectorKey
	 * @return
	 */
	public static By getBySelector(String selectorKey) {
		By bySelector = null;
		
		if(selectorKey != null) {	    
		    String sel = Helper.getPropertyValue(selectorKey);
		    
		    if(sel != null && !sel.equals("")){
		    	sel = sel.trim();
		    	if(sel.startsWith("id:")) {
			    	bySelector = By.id(sel.replace("id:", ""));
			    }
			    
			    if(sel.startsWith("name:")) {
			    	bySelector = By.name(sel.replace("name:", ""));
			    }
			    
			    if(sel.startsWith("css:")) {
			    	bySelector = By.cssSelector(sel.replace("css:", ""));
			    }
			    
			    if(sel.startsWith("class:")) {
			    	bySelector = By.className(sel.replace("class:", ""));
			    }
			    
			    if(sel.startsWith("ltx:")) {
			    	bySelector = By.linkText(sel.replace("ltx:", ""));
			    }
			    
			    if(sel.startsWith("pltx:")) {
			    	bySelector = By.partialLinkText(sel.replace("ltx:", ""));
			    }
			    
			    if(sel.startsWith("tag:")) {
			    	bySelector = By.tagName(sel.replace("tag:", ""));
			    }
			    
			    if(sel.startsWith("xpath:")) {
			    	bySelector = By.tagName(sel.replace("xpath:", ""));
			    }
			}
		    
	    }
		return bySelector;
   }
	
	/**
     * 
     * @return
     * @throws IOException
     */
	public static WebDriver initWebDriver() {
		WebDriver webDriver = null;
		
		String browserMode = Helper.getPropertyValue(Constants.BROSER_MODE);
		
		if("1".equals(browserMode)){
			//Run on firefox
			webDriver = new FirefoxDriver();
		}
		
		if("2".equals(browserMode)){
			//Run on Chrome
		    System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		    webDriver = new ChromeDriver();
		}
		
		if("3".equals(browserMode)){
			//Run on IE
			System.setProperty("webdriver.ie.driver", "IEdriver.exe");
			webDriver = new InternetExplorerDriver();
		}
		
		return webDriver;
		
	}
	
	
	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean convertStringToBoolean(String str){
		try{
			if(str!= null && !str.equals("")){
				return Boolean.parseBoolean(str);
			}else{
				return false;
			}
		}catch(Exception e){
			if(str.toLowerCase().startsWith("t")){
				return true;
			}else{
				return false;
			}
		}
	}


	/**
	 * 
	 * @param testCaseData
	 * @return
	 */
	public static boolean checkEmptyRow(List<String> list) {
		for(String e : list){
			if(!e.equals("")){
				return false;
			}
		}
		return true;
	}
	
	
	/**
	 * 
	 * @param text
	 * @return
	 * Get number of money from text display on pages.
	 */
	public static long splitPriceFromText(String text){
		String[] arr = text.split("VND");
		return Long.parseLong(formatStringToNumber(arr[0]));
	}
	
	
	/**
	 * 
	 * @param ftext
	 * @return
	 * Get number of money for formatted money string.
	 */
	public static String formatStringToNumber(String ftext){
		String rs = "";
		for(int i =0; i < ftext.length(); i++){
			String c = ftext.substring(i,i+1);
			try {
				Integer.parseInt(c);
				rs = rs.concat(c);
			} catch(NumberFormatException e){
				//not a valid number--do nothing
			}
		}
		return rs;
	}
}
