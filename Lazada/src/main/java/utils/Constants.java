package utils;

/**
 * 
 * @author hongdang
 *
 */
public class Constants {
	
	public static final String BROSER_MODE = "browser.mode";
	public static final String RESET_TEST_ENV = "reset.test.environment";
	public static final String WEBSITE_DOMAIN = "website.domain";
	
	public static final String BROSER_MODE_DEFAULT = "1";
	public static final String RESET_TEST_ENV_DEFAULT = "false";
	public static final String WEBSITE_DOMAIN_DEFAULT = "http://www.lazada.vn";
	
	//****POP UP*****
	public static final String WINDOW_POPUP = "window.popup";
	public static final String WINDOW_POPUP_CLOSE_BTN = "window.popup.close.btn";
	
	//*****SIGN UP PAGE*****
	public static final String SIGN_UP_NAME = "sign.up.name";
	public static final String SIGN_UP_EMAIL = "sign.up.email";
	public static final String SIGN_UP_PASSWORD = "sign.up.password";
	public static final String SIGN_UP_CONFIRMPASS = "sign.up.confirmpass";
	public static final String SIGN_UP_SENDBTN = "sign.up.btn.submit";
	public static final String SIGN_UP_TAB_TITLE = "sign.up.tab.title";
	public static final String SIGN_UP_ERR_MSGS= "sign.up.errorMsgs";
	public static final String SIGN_UP_REQ_MSG = "sign.up.req.msg";
	public static final String SIGN_UP_PAGE_TITLE = "sign.up.page.title";
	public static final String SIGN_UP_PAGE_LABEL = "sign.up.label";
	
	
	//*****PRODUCT PAGE*****
	public static final String PRODUCT_TITLE = "product.title";
	public static final String PRODUCT_PRICE = "product.price";
	public static final String PRODUCT_BUY_BTN = "product.buy.btn";
	public static final String PRODUCT_AD = "product.ad";
	
	
	//****TOP MENU PAGE*****
	public static final String REGISTER_FORM_LINK = "top.menu.register.form.link";
	public static final String CART_PAGE_LINK = "top.menu.cart.page.link";
	
	//*****PRODUCT SHOPPING CART ITEM PAGE******
	public static final String SHOPPING_CART_ITEM_SRC = "product.shoppingcart.item.src";
	public static final String SHOPPING_CART_ITEM_DES = "product.shoppingcart.item.des";
	public static final String SHOPPING_CART_ITEM_PRICE = "product.shoppingcart.item.price";
	public static final String SHOPPING_CART_ITEM_COUNT = "product.shoppingcart.item.count";
	
	
	//*****SHOPPING CART PAGE******
	public static final String SHOPPING_CART_PRODUCT_LIST = "shoppingcart.page.product.list";
	public static final String SHOPPING_CART_SUB_TOTAL = "shoppingcart.page.sub.total";
	public static final String SHOPPING_CART_DELETE_LINKS = "shoppingcart.page.delete.link";
	public static final String SHOPPING_CART_NOTIFY_MSG="shoppingcart.page.notify.msg";
	
}
