package testcases.signup;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.SignUpPage;
import pages.TopMenuPage;
import testcases.common.AbstractTest;
import testcases.common.InitTest;

public class SignUpTest extends AbstractTest{
	private TopMenuPage topeMenuPage;
	private SignUpPage signUpPage;
	
	
	@BeforeClass
	public void init(){
		topeMenuPage = new TopMenuPage(InitTest.driver);
	}
	
	
	
	@Test (priority = 1)
	@Parameters({"name","email","password"})
	public void validateErrorMsgIfNoInputConfirmPassword(String name, String email, String password){
		try {
			//***Step1 : Open 'Sign Up' page.
			signUpPage = topeMenuPage.openNewAccountForm();
			
			if(signUpPage != null){
				//***Step 2 : Add test data into fields except 'Confirm pass'.
				signUpPage.sendDataIntoNameField(name);
				signUpPage.sendDataIntoEmailField(email);
				signUpPage.sendDataIntoPassField(password);
				
				//***Step 3: Click Submit button
				signUpPage.clickSendBtn();
				
				/***
				 * Check expected result
				 * Require error message displays on page.
				 */
				boolean rs = signUpPage.checkDisplayOfRequiredErrorMsgForConfirmPass();
				Assert.assertEquals(rs, true);
			}else{
				Assert.fail();
			}
			
			signUpPage.resetTestEnvironment();
		
		}catch(Exception e){
			//Test Case fail if there is any exception occur.
			e.printStackTrace();
			Assert.fail();
		}
	}
}
