package testcases.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.log4testng.Logger;

/**
 * 
 * @author hongdang
 *
 */
public abstract class AbstractTest {
	
	protected List<List<String>> data;
	protected Logger logger = Logger.getLogger(AbstractTest.class);
	
	
	/**
	 * 
	 * @param excelPath
	 * Load test data from excel file
	 */
	public void loadDataFromExcelFile(String excelPath){
		data = new ArrayList<List<String>>();
		
		try {
			FileInputStream file = new FileInputStream(new File(this.getClass().getResource(excelPath).toURI()));
			
			//Get the workbook instance for XLS file
			XSSFWorkbook workbook = new XSSFWorkbook(file);
		 
		    //Get first sheet from the workbook
		    XSSFSheet sheet = workbook.getSheetAt(0);
		     
		    //Iterate through each rows from first sheet
		    Iterator<Row> rowIterator = sheet.iterator();
		    //Omit the header - two first rows
		    rowIterator.next();
		    rowIterator.next();
		    
		    while(rowIterator.hasNext()) {
		    	Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
		    	
		    	List<String> testcaseData = new ArrayList<String>();
		    	while(cellIterator.hasNext()){
		    		String data = cellIterator.next().getStringCellValue();
		    		if("null".equals(data)){
		    			testcaseData.add("");
		    		}else{
		    			testcaseData.add(data);
		    		}
		    	}
		    	
		    	//Put data of test case into Map
		    	data.add(testcaseData);
		    	
		    }
		    
		    file.close();
		    
		} catch (FileNotFoundException e) {
			logger.error("Can not find out the data file");
		} catch (IOException e) {
			logger.error(e.getMessage());
		} catch (URISyntaxException e) {
			logger.error(e.getMessage());
		}
	}
	
}
