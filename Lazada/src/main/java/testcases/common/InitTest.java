package testcases.common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import utils.Constants;
import utils.Helper;

/**
 * 
 * @author hongdang
 * This test runs at the beginning of each test suite
 * Purpose: Initialize web driver.
 *
 */
public class InitTest {
	public static WebDriver driver;
	
	
	/**
	 * Initialize web driver before running test suite
	 */
	@BeforeSuite
	public void initWebDriver(){
		if(driver == null){
			driver = Helper.initWebDriver();
		}
	}
	
	
	@Test
	public void openLazadaPage(){
		driver.get(Constants.WEBSITE_DOMAIN_DEFAULT);
	}
	
	
	/**
	 * Shutdown web driver after finishing test suite
	 */
	@AfterSuite
	public void shutdownWebDriver(){
		driver.quit();
	}

}
