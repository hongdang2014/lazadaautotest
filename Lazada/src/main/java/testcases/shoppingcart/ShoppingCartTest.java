package testcases.shoppingcart;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import pages.ProductPage;
import pages.ProductShoppingCartItemPage;
import pages.ShoppingCartPage;
import pages.TopMenuPage;
import testcases.common.AbstractTest;
import testcases.common.InitTest;

public class ShoppingCartTest extends AbstractTest{
	private ProductPage productPage;
	private ShoppingCartPage shoppingCartPage;
	private TopMenuPage topeMenuPage;
	
	
	@BeforeClass
	public void init(){
		topeMenuPage = new TopMenuPage(InitTest.driver);
	}
	
	
	
	@Test (priority = 1)
	@Parameters({"productLink1","productLink2"})
	public void validateSubTotalWhenChangeQty(String productLink1, String productLink2){
		try {
			//***Step 1 : Add 2 products into shopping cart***
			productPage = topeMenuPage.openProductPage(productLink1);
			if(productPage == null){
				//Can not open product page, test case fail
				Assert.fail();
			}
			
			shoppingCartPage = productPage.buyProduct();
			if(shoppingCartPage == null){
				//Shopping Cart Page doesn't open after user clicks 'Buy' button.
				Assert.fail();
			}
			
			topeMenuPage.openProductPage(productLink2);
			productPage.buyProduct();
			
			//Fetch web elements on opening shopping cart page.
			shoppingCartPage.loadProductsElement();
			
			
			//***Step 2 : Increase amount of product 1 on opening shopping cart page***
			ProductShoppingCartItemPage item1 = shoppingCartPage.getProducts().get(0);
			int newNumber = Integer.parseInt(item1.getNumberOfProduct()) + 1;
			item1.setNumberOfProduct(String.valueOf(newNumber));
			
			//Wait for page finish reloading.
			shoppingCartPage.waitForPageReload();
			shoppingCartPage.loadProductsElement();
			
			
			//***Step 3: Increase amount of product 2 on opening shopping cart page***
			ProductShoppingCartItemPage item2 = shoppingCartPage.getProducts().get(1);
			int newNumber2 = Integer.parseInt(item2.getNumberOfProduct()) + 2;
			item2.setNumberOfProduct(String.valueOf(newNumber2));
			
			//Wait for page finish reloading.
			shoppingCartPage.waitForPageReload();
			shoppingCartPage.loadProductsElement();
			
			/***Check expected result***
			 *  total money of all chosen product 1 is counted correctly.
			 *  total money of all chosen product 2 is counted correctly.
			 *  total money of shopping cart is counted correctly
			 */
			
			//Validate subtotal of product 1 after increasing number of products.
			Assert.assertTrue(shoppingCartPage.getProducts().get(0).validateTotalOfProductPrice());
			
			//Validate subtotal of product 2 after increasing number of products.
			Assert.assertTrue(shoppingCartPage.getProducts().get(1).validateTotalOfProductPrice());
			
			//Validate total money of shopping cart after increasing number of products.
			Assert.assertTrue(shoppingCartPage.validateShoppingCartTotalPrice());
				
			//Remove chosen product in shopping cart.
			shoppingCartPage.resetTestEnvironment(topeMenuPage);
		
		}catch(Exception e){
			//Test case fail if there is any exception occurs.
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	
	
	@Test (priority = 2)
	@Parameters("productCheckLink")
	public void validateProductInformationOnCartPage(String productCheckLink){
		try {
			//***Step1 : Open product page
			productPage = topeMenuPage.openProductPage(productCheckLink);
			if(productPage == null){
				//Can not open product page. Test case fail
				Assert.fail();
			}
			
			//Get product information to compare
			String title = productPage.getProductTitle();
			String price = productPage.getProductPrice();
			
			//***Step2 : Add product into shopping cart and open shopping cart page.
			shoppingCartPage = productPage.buyProduct();
			
			if(shoppingCartPage == null){
				//Shopping Cart Page doesn't open after user clicks 'Buy' button.
				Assert.fail();
			}
			shoppingCartPage.loadProductsElement();
			
			/**
			 * Check expected result
			 * Product information display on 'Product' page
			 * are same with product information display on Shopping Cart page.
			 * 
			 */
			ProductShoppingCartItemPage item =  shoppingCartPage.getProducts().get(0);
			if(title.equals(item.getProductDescription()) && price.equals(item.getProductPrice())){
				Assert.assertTrue(true);
			}else{
				Assert.assertTrue(false);
			}
			
			////Remove chosen product in shopping cart.
			shoppingCartPage.resetTestEnvironment(topeMenuPage);
		
		}catch(Exception e){
			//Test case fail if there is any exception occurs.
			e.printStackTrace();
			Assert.fail();
		}
	}
	
}
		